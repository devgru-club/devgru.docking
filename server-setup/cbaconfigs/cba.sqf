// ACE Advanced Ballistics
force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force ace_advanced_ballistics_bulletTraceEnabled = true;
force ace_advanced_ballistics_enabled = true;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force ace_advanced_ballistics_simulationInterval = 0.00713011;

// ACE Advanced Fatigue
force ace_advanced_fatigue_enabled = true;
force ace_advanced_fatigue_enableStaminaBar = true;
force ace_advanced_fatigue_loadFactor = 0.1;
force ace_advanced_fatigue_performanceFactor = 5;
force ace_advanced_fatigue_recoveryFactor = 5;
force ace_advanced_fatigue_swayFactor = 0.1;
force ace_advanced_fatigue_terrainGradientFactor = 0.1;

// ACE Advanced Throwing
force ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
force ace_advanced_throwing_showMouseControls = true;
force ace_advanced_throwing_showThrowArc = true;

// ACE Arsenal
force ace_arsenal_allowDefaultLoadouts = true;
force ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
force ace_arsenal_enableIdentityTabs = true;
force ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
force ace_arsenal_fontHeight = 4.5;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 2;
force ace_captives_requireSurrenderAi = false;

// ACE Common
force ace_common_allowFadeMusic = true;
force ace_common_checkPBOsAction = 0;
force ace_common_checkPBOsCheckAll = false;
force ace_common_checkPBOsWhitelist = "[]";
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [0.317885,0.689245,0.325311,1];
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 1;
force ace_noradio_enabled = true;
ace_parachute_hideAltimeter = false;

// ACE Cook off
force ace_cookoff_ammoCookoffDuration = 0.103979;
force ace_cookoff_enable = false;
force ace_cookoff_enableAmmobox = true;
force ace_cookoff_enableAmmoCookoff = true;
force ace_cookoff_probabilityCoef = 0.141116;

// ACE Explosives
force ace_explosives_explodeOnDefuse = false;
force ace_explosives_punishNonSpecialists = false;
force ace_explosives_requireSpecialist = false;

// ACE Fragmentation Simulation
force ace_frag_enabled = true;
force ace_frag_maxTrack = 2;
force ace_frag_maxTrackPerFrame = 2;
force ace_frag_reflectionsEnabled = true;
force ace_frag_spallEnabled = true;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showInThirdPerson = false;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = true;
force ace_hearing_disableEarRinging = true;
force ace_hearing_earplugsVolume = 0.310457;
force ace_hearing_enableCombatDeafness = true;
force ace_hearing_enabledForZeusUnits = true;
force ace_hearing_unconsciousnessVolume = 0.199049;

// ACE Interaction
force ace_interaction_disableNegativeRating = true;
ace_interaction_enableMagazinePassing = true;
force ace_interaction_enableTeamManagement = true;

// ACE Interaction Menu
force ace_gestures_showOnInteractionMenu = 2;
force ace_interact_menu_actionOnKeyRelease = true;
force ace_interact_menu_addBuildingActions = true;
force ace_interact_menu_alwaysUseCursorInteraction = true;
force ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,1,1];
ace_interact_menu_colorShadowMin = [0.919489,0.288176,0,0.25];
ace_interact_menu_colorTextMax = [0,0,1,1];
ace_interact_menu_colorTextMin = [1,1,0,1];
force ace_interact_menu_cursorKeepCentered = false;
force ace_interact_menu_menuAnimationSpeed = 2;
force ace_interact_menu_menuBackground = 1;
ace_interact_menu_selectorColor = [1,0,0];
force ace_interact_menu_shadowSetting = 2;
force ace_interact_menu_textSize = 2;
force ace_interact_menu_useListMenu = true;

// ACE Logistics
force ace_cargo_enable = true;
force ace_cargo_loadTimeCoefficient = 0.0594153;
force ace_cargo_paradropTimeCoefficent = 2.5;
force ace_rearm_level = 0;
force ace_rearm_supply = 0;
force ace_refuel_hoseLength = 20;
force ace_refuel_rate = 10;
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force ace_repair_engineerSetting_fullRepair = 0;
force ace_repair_engineerSetting_repair = 0;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 0;
force ace_repair_repairDamageThreshold = 0.8;
force ace_repair_repairDamageThreshold_engineer = 0.518419;
force ace_repair_wheelRepairRequiredItems = 0;

// ACE Magazine Repack
force ace_magazinerepack_timePerAmmo = 1.5;
force ace_magazinerepack_timePerBeltLink = 8;
force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force ace_map_BFT_Enabled = true;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 0.1;
force ace_map_BFT_ShowPlayerNames = true;
force ace_map_DefaultChannel = 1;
force ace_map_mapGlow = true;
force ace_map_mapIllumination = true;
force ace_map_mapLimitZoom = false;
force ace_map_mapShake = true;
force ace_map_mapShowCursorCoordinates = true;
ace_markers_moveRestriction = 0;

// ACE Map Gestures
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force ace_map_gestures_enabled = true;
force ace_map_gestures_interval = 0.03;
force ace_map_gestures_maxRange = 7;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force ace_medical_ai_enabledFor = 0;
force ace_medical_AIDamageThreshold = 0.529232;
force ace_medical_allowLitterCreation = true;
force ace_medical_allowUnconsciousAnimationOnTreatment = true;
force ace_medical_amountOfReviveLives = -1;
force ace_medical_bleedingCoefficient = 0.5;
force ace_medical_blood_enabledFor = 1;
force ace_medical_consumeItem_PAK = 1;
force ace_medical_consumeItem_SurgicalKit = 0;
force ace_medical_convertItems = 0;
force ace_medical_delayUnconCaptive = 9.53654;
force ace_medical_enableAdvancedWounds = true;
force ace_medical_enableFor = 1;
force ace_medical_enableOverdosing = true;
force ace_medical_enableRevive = 2;
force ace_medical_enableScreams = true;
force ace_medical_enableUnconsciousnessAI = 0;
force ace_medical_enableVehicleCrashes = true;
force ace_medical_healHitPointAfterAdvBandage = true;
force ace_medical_increaseTrainingInLocations = true;
force ace_medical_keepLocalSettingsSynced = true;
force ace_medical_level = 2;
force ace_medical_litterCleanUpDelay = 300;
force ace_medical_litterSimulationDetail = 3;
force ace_medical_maxReviveTime = 30;
force ace_medical_medicSetting = 2;
force ace_medical_medicSetting_basicEpi = 0;
force ace_medical_medicSetting_PAK = 1;
force ace_medical_medicSetting_SurgicalKit = 0;
force ace_medical_menu_allow = 1;
force ace_medical_menu_maxRange = 10;
force ace_medical_menu_openAfterTreatment = true;
force ace_medical_menu_useMenu = 1;
force ace_medical_menuTypeStyle = 0;
force ace_medical_menuTypeStyleSelf = true;
force ace_medical_moveUnitsFromGroupOnUnconscious = false;
force ace_medical_painCoefficient = 0.5;
force ace_medical_painEffectType = 1;
force ace_medical_painIsOnlySuppressed = true;
force ace_medical_playerDamageThreshold = 2.5;
force ace_medical_preventInstaDeath = true;
force ace_medical_remoteControlledAI = true;
force ace_medical_useCondition_PAK = 1;
force ace_medical_useCondition_SurgicalKit = 1;
force ace_medical_useLocation_basicEpi = 0;
force ace_medical_useLocation_PAK = 3;
force ace_medical_useLocation_SurgicalKit = 0;

// ACE Mk6 Mortar
force ace_mk6mortar_airResistanceEnabled = true;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = true;

// ACE Name Tags
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
force ace_nametags_playerNamesMaxAlpha = 0.8;
force ace_nametags_playerNamesViewDistance = 5;
force ace_nametags_showCursorTagForVehicles = true;
ace_nametags_showNamesForAI = true;
ace_nametags_showPlayerNames = 0;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
force ace_nightvision_aimDownSightsBlur = 0.101011;
force ace_nightvision_disableNVGsWithSights = false;
force ace_nightvision_effectScaling = 1;
force ace_nightvision_fogScaling = 0.145573;
force ace_nightvision_noiseScaling = 0.145573;
ace_nightvision_shutterEffects = true;

// ACE Overheating
ace_overheating_displayTextOnJam = true;
force ace_overheating_enabled = true;
force ace_overheating_overheatingDispersion = true;
ace_overheating_showParticleEffects = true;
ace_overheating_showParticleEffectsForEveryone = true;
force ace_overheating_unJamFailChance = 0.0505054;
force ace_overheating_unJamOnreload = true;

// ACE Pointing
force ace_finger_enabled = true;
ace_finger_indicatorColor = [0,0,1,1];
ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 50;

// ACE Pylons
force ace_pylons_enabledForZeus = true;
force ace_pylons_enabledFromAmmoTrucks = true;
force ace_pylons_rearmNewPylons = true;
force ace_pylons_requireEngineer = false;
force ace_pylons_requireToolkit = true;
force ace_pylons_searchDistance = 50;
force ace_pylons_timePerPylon = 3.3262;

// ACE Quick Mount
force ace_quickmount_distance = 3;
force ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 0;
force ace_quickmount_speed = 18;

// ACE Respawn
force ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = true;

// ACE Scopes
force ace_scopes_correctZeroing = true;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = true;
force ace_scopes_defaultZeroRange = 100;
force ace_scopes_enabled = true;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
force ace_scopes_overwriteZeroRange = false;
force ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Spectator
force ace_spectator_enableAI = false;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force ace_switchunits_enableSafeZone = true;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 100;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE Tagging
force ace_tagging_quickTag = 3;

// ACE TFAR Radio Setter
force CHTR_TFAR_Setter_Layout = false;
force CHTR_TFAR_Setter_Shortcut = true;
force CHTR_TFAR_Setter_ShowLR = true;
force CHTR_TFAR_Setter_ShowSR = true;

// ACE Uncategorized
force ace_fastroping_requireRopeItems = true;
force ace_gforces_enabledFor = 1;
force ace_hitreactions_minDamageToTrigger = 0.1;
force ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_microdagr_mapDataAvailable = 2;
force ace_microdagr_waypointPrecision = 3;
ace_optionsmenu_showNewsOnMainMenu = true;
force ace_overpressure_distanceCoefficient = 1;

// ACE User Interface
force ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = true;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
ace_ui_firingMode = true;
ace_ui_groupBar = true;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
force ace_vehiclelock_defaultLockpickStrength = 1;
force ace_vehiclelock_lockVehicleInventory = true;
force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE View Distance Limiter
force ace_viewdistance_enabled = true;
force ace_viewdistance_limitViewDistance = 12000;
force ace_viewdistance_objectViewDistanceCoeff = 6;
force ace_viewdistance_viewDistanceAirVehicle = 14;
force ace_viewdistance_viewDistanceLandVehicle = 4;
force ace_viewdistance_viewDistanceOnFoot = 3;

// ACE Weapons
ace_common_persistentLaserEnabled = true;
force ace_laserpointer_enabled = true;
ace_reload_displayText = true;
ace_weaponselect_displayText = true;

// ACE Weather
force ace_weather_enabled = false;
force ace_weather_updateInterval = 300;
force ace_weather_windSimulation = true;

// ACE Wind Deflection
force ace_winddeflection_enabled = true;
force ace_winddeflection_simulationInterval = 0.00713011;
force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force ace_zeus_autoAddObjects = true;
force ace_zeus_canCreateZeus = -1;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 1;
force ace_zeus_zeusAscension = true;
force ace_zeus_zeusBird = true;

// ACEX Field Rations
force acex_field_rations_affectAdvancedFatigue = true;
force acex_field_rations_enabled = true;
force acex_field_rations_hudShowLevel = 0;
force acex_field_rations_hudTransparency = -1;
force acex_field_rations_hudType = 0;
force acex_field_rations_hungerSatiated = 3.5;
force acex_field_rations_thirstQuenched = 3;
force acex_field_rations_timeWithoutFood = 16;
force acex_field_rations_timeWithoutWater = 8;

// ACEX Fortify
acex_fortify_settingHint = 2;

// ACEX Headless
force acex_headless_delay = 120;
force acex_headless_enabled = false;
force acex_headless_endMission = 0;
force acex_headless_log = false;
force acex_headless_transferLoadout = 1;

// ACEX Sitting
force acex_sitting_enable = true;

// ACEX View Restriction
force acex_viewrestriction_mode = 0;
force acex_viewrestriction_modeSelectiveAir = 0;
force acex_viewrestriction_modeSelectiveFoot = 0;
force acex_viewrestriction_modeSelectiveLand = 0;
force acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = true;

// ACEX Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// Achilles - Available Factions
Achilles_var_B_UnitedStatesUSNAVY_Clandestine = true;
Achilles_var_BLU_CTRG_F = true;
Achilles_var_BLU_F = true;
Achilles_var_BLU_G_F = true;
Achilles_var_BLU_GEN_F = true;
Achilles_var_BLU_T_F = true;
Achilles_var_BLU_W_F = true;
Achilles_var_BRPMC = true;
Achilles_var_CIV_F = true;
Achilles_var_CIV_F_AFRICA = true;
Achilles_var_CIV_F_ASIA = true;
Achilles_var_CIV_F_EUROPE = true;
Achilles_var_CIV_F_TANOA = true;
Achilles_var_CIV_IDAP_F = true;
Achilles_var_CUP_B_CDF = true;
Achilles_var_CUP_B_CZ = true;
Achilles_var_CUP_B_GB = true;
Achilles_var_CUP_B_GER = true;
Achilles_var_CUP_B_RNZN = true;
Achilles_var_CUP_B_US = true;
Achilles_var_CUP_B_US_Army = true;
Achilles_var_CUP_B_USMC = true;
Achilles_var_CUP_C_CHERNARUS = true;
Achilles_var_CUP_C_RU = true;
Achilles_var_CUP_C_SAHRANI = true;
Achilles_var_CUP_C_Special = true;
Achilles_var_CUP_C_TK = true;
Achilles_var_CUP_I_NAPA = true;
Achilles_var_CUP_I_PMC_ION = true;
Achilles_var_CUP_I_RACS = true;
Achilles_var_CUP_I_TK_GUE = true;
Achilles_var_CUP_I_UN = true;
Achilles_var_CUP_O_ChDKZ = true;
Achilles_var_CUP_O_RU = true;
Achilles_var_CUP_O_SLA = true;
Achilles_var_CUP_O_TK = true;
Achilles_var_CUP_O_TK_MILITIA = true;
Achilles_var_DSF_ABUS = true;
Achilles_var_DSF_AFMIL = true;
Achilles_var_DSF_BOKO = true;
Achilles_var_DSF_CART = true;
Achilles_var_DSF_Civilians = true;
Achilles_var_DSF_IDF = true;
Achilles_var_DSF_ISIS = true;
Achilles_var_DSF_TAL = true;
Achilles_var_FIR_AWS_ENEMY_F = true;
Achilles_var_FIR_AWS_FRND_F = true;
Achilles_var_IND_C_F = true;
Achilles_var_IND_E_F = true;
Achilles_var_IND_F = true;
Achilles_var_IND_G_F = true;
Achilles_var_IND_L_F = true;
Achilles_var_Interactive_F = true;
Achilles_var_JSOC = true;
Achilles_var_LOP_AA = true;
Achilles_var_LOP_AFR = true;
Achilles_var_LOP_AFR_Civ = true;
Achilles_var_LOP_AFR_OPF = true;
Achilles_var_LOP_AM = true;
Achilles_var_LOP_AM_OPF = true;
Achilles_var_LOP_BH = true;
Achilles_var_LOP_CDF = true;
Achilles_var_LOP_ChDKZ = true;
Achilles_var_LOP_CHR_Civ = true;
Achilles_var_LOP_GRE = true;
Achilles_var_LOP_IA = true;
Achilles_var_LOP_IRA = true;
Achilles_var_LOP_IRAN = true;
Achilles_var_LOP_ISTS = true;
Achilles_var_LOP_ISTS_OPF = true;
Achilles_var_LOP_NAPA = true;
Achilles_var_LOP_NK = true;
Achilles_var_LOP_PESH = true;
Achilles_var_LOP_PESH_IND = true;
Achilles_var_LOP_PMC = true;
Achilles_var_LOP_RACS = true;
Achilles_var_LOP_SLA = true;
Achilles_var_LOP_SYR = true;
Achilles_var_LOP_TAK_Civ = true;
Achilles_var_LOP_TKA = true;
Achilles_var_LOP_TRK = true;
Achilles_var_LOP_UA = true;
Achilles_var_LOP_UKR = true;
Achilles_var_LOP_UN = true;
Achilles_var_LOP_US = true;
Achilles_var_LOP_UVF = true;
Achilles_var_mas_cia_jsog_a = true;
Achilles_var_mas_cia_jsog_b = true;
Achilles_var_mas_cia_jsog_c = true;
Achilles_var_mas_cia_jsog_d = true;
Achilles_var_mas_cia_jsog_e = true;
Achilles_var_mas_cia_jsog_f = true;
Achilles_var_mas_cia_jsog_m = true;
Achilles_var_mas_cia_jsog_w = true;
Achilles_var_mas_usl_amulti = true;
Achilles_var_mas_usl_bwood = true;
Achilles_var_mas_usl_caor1 = true;
Achilles_var_mas_usl_daor2 = true;
Achilles_var_mas_usl_ewint = true;
Achilles_var_mas_usl_frang = true;
Achilles_var_mas_usl_gspec = true;
Achilles_var_mas_usl_hcvrt = true;
Achilles_var_mas_usl_idiver = true;
Achilles_var_mas_usl_jparas = true;
Achilles_var_mas_usl_kswat = true;
Achilles_var_mas_usl_lfbi = true;
Achilles_var_mas_usl_mct = true;
Achilles_var_NSWDG_Gen3_Units = true;
Achilles_var_OPF_F = true;
Achilles_var_OPF_G_F = true;
Achilles_var_OPF_R_F = true;
Achilles_var_OPF_T_F = true;
Achilles_var_OPF_V_F = true;
Achilles_var_rhs_faction_insurgents = true;
Achilles_var_rhs_faction_msv = true;
Achilles_var_rhs_faction_rva = true;
Achilles_var_rhs_faction_socom = true;
Achilles_var_rhs_faction_tv = true;
Achilles_var_rhs_faction_usaf = true;
Achilles_var_rhs_faction_usarmy = true;
Achilles_var_rhs_faction_usarmy_d = true;
Achilles_var_rhs_faction_usarmy_wd = true;
Achilles_var_rhs_faction_usmc = true;
Achilles_var_rhs_faction_usn = true;
Achilles_var_rhs_faction_vdv = true;
Achilles_var_rhs_faction_vdv_45 = true;
Achilles_var_rhs_faction_vmf = true;
Achilles_var_rhs_faction_vpvo = true;
Achilles_var_rhs_faction_vv = true;
Achilles_var_rhs_faction_vvs = true;
Achilles_var_rhs_faction_vvs_c = true;
Achilles_var_rhsgref_faction_cdf_air = true;
Achilles_var_rhsgref_faction_cdf_air_b = true;
Achilles_var_rhsgref_faction_cdf_ground = true;
Achilles_var_rhsgref_faction_cdf_ground_b = true;
Achilles_var_rhsgref_faction_cdf_ng = true;
Achilles_var_rhsgref_faction_cdf_ng_b = true;
Achilles_var_rhsgref_faction_chdkz = true;
Achilles_var_rhsgref_faction_chdkz_g = true;
Achilles_var_rhsgref_faction_hidf = true;
Achilles_var_rhsgref_faction_nationalist = true;
Achilles_var_rhsgref_faction_tla = true;
Achilles_var_rhsgref_faction_un = true;
Achilles_var_rhssaf_faction_airforce = true;
Achilles_var_rhssaf_faction_airforce_opfor = true;
Achilles_var_rhssaf_faction_army = true;
Achilles_var_rhssaf_faction_army_opfor = true;
Achilles_var_rhssaf_faction_un = true;
Achilles_var_RWG = true;
Achilles_var_SSQN_Faction = true;
Achilles_var_SSQN_ISF_Faction = true;
Achilles_var_TF160 = true;
Achilles_var_Tier1_SMU = true;
Achilles_var_USA = true;
Achilles_var_USAF = true;
Achilles_var_usaf_afsoc = true;
Achilles_var_USAF_SFS_Pilots = true;
Achilles_var_USMC = true;
Achilles_var_USN = true;
Achilles_var_USNavy = true;
Achilles_var_Virtual_F = true;
Achilles_var_VSM_GP = true;

// Achilles - Available Modules
force Achilles_var_Achilles_ACE_Heal_Module = true;
force Achilles_var_Achilles_ACE_Injury_Module = true;
force Achilles_var_Achilles_AddECM_Module = true;
force Achilles_var_Achilles_Animation_Module = true;
force Achilles_var_Achilles_Attach_To_Module = true;
force Achilles_var_Achilles_Bind_Variable_Module = true;
force Achilles_var_Achilles_Buildings_Destroy_Module = true;
force Achilles_var_Achilles_Buildings_LockDoors_Module = true;
force Achilles_var_Achilles_Buildings_ToggleLight_Module = true;
force Achilles_var_Achilles_CAS_Module = true;
force Achilles_var_Achilles_Change_Ability_Module = true;
force Achilles_var_Achilles_Change_Altitude_Module = true;
force Achilles_var_Achilles_Chatter_Module = true;
force Achilles_var_Achilles_Create_Universal_Target_Module = true;
force Achilles_var_Achilles_DevTools_FunctionViewer = true;
force Achilles_var_Achilles_DevTools_ShowInAnimViewer = true;
force Achilles_var_Achilles_DevTools_ShowInConfig = true;
force Achilles_var_Achilles_Earthquake_Module = true;
force Achilles_var_Achilles_Hide_Objects_Module = true;
force Achilles_var_Achilles_IED_Module = true;
force Achilles_var_Achilles_Make_Invincible_Module = true;
force Achilles_var_Achilles_Module_Arsenal_AddFull = true;
force Achilles_var_Achilles_Module_Arsenal_CopyToClipboard = true;
force Achilles_var_Achilles_Module_Arsenal_CreateCustom = true;
force Achilles_var_Achilles_Module_Arsenal_Paste = true;
force Achilles_var_Achilles_Module_Arsenal_Remove = true;
force Achilles_var_Achilles_Module_Change_Side_Relations = true;
force Achilles_var_Achilles_Module_Equipment_Attach_Dettach_Effect = true;
force Achilles_var_Achilles_Module_FireSupport_CASBomb = true;
force Achilles_var_Achilles_Module_FireSupport_CASGun = true;
force Achilles_var_Achilles_Module_FireSupport_CASGunMissile = true;
force Achilles_var_Achilles_Module_FireSupport_CASMissile = true;
force Achilles_var_Achilles_Module_Manage_Advanced_Compositions = true;
force Achilles_var_Achilles_Module_Player_Set_Frequencies = true;
force Achilles_var_Achilles_Module_Rotation = true;
force Achilles_var_Achilles_Module_Spawn_Advanced_Composition = true;
force Achilles_var_Achilles_Module_Spawn_Carrier = true;
force Achilles_var_Achilles_Module_Spawn_Destroyer = true;
force Achilles_var_Achilles_Module_Spawn_Effects = true;
force Achilles_var_Achilles_Module_Spawn_Explosives = true;
force Achilles_var_Achilles_Module_Spawn_Intel = true;
force Achilles_var_Achilles_Module_Supply_Drop = true;
force Achilles_var_Achilles_Module_Zeus_AssignZeus = true;
force Achilles_var_Achilles_Module_Zeus_SwitchUnit = true;
force Achilles_var_Achilles_Nuke_Module = true;
force Achilles_var_Achilles_Set_Date_Module = true;
force Achilles_var_Achilles_Set_Height_Module = true;
force Achilles_var_Achilles_Set_Weather_Module = true;
force Achilles_var_Achilles_Sit_On_Chair_Module = true;
force Achilles_var_Achilles_SuicideBomber_Module = true;
force Achilles_var_Achilles_Suppressive_Fire_Module = true;
force Achilles_var_Achilles_Toggle_Simulation_Module = true;
force Achilles_var_Achilles_Transfer_Ownership_Module = true;
force Achilles_var_Ares_Artillery_Fire_Mission_Module = true;
force Achilles_var_Ares_Module_Bahaviour_Garrison_Nearest = true;
force Achilles_var_Ares_Module_Bahaviour_SurrenderUnit = true;
force Achilles_var_Ares_Module_Bahaviour_UnGarrison = true;
force Achilles_var_Ares_Module_Behaviour_Patrol = true;
force Achilles_var_Ares_Module_Behaviour_Search_Nearby_And_Garrison = true;
force Achilles_var_Ares_Module_Behaviour_Search_Nearby_Building = true;
force Achilles_var_Ares_Module_Dev_Tools_Create_Mission_SQF = true;
force Achilles_var_Ares_Module_Dev_Tools_Execute_Code = true;
force Achilles_var_Ares_Module_Equipment_Flashlight_IR_ON_OFF = true;
force Achilles_var_Ares_Module_Equipment_NVD_TACLIGHT_IR = true;
force Achilles_var_Ares_Module_Equipment_Turret_Optics = true;
force Achilles_var_Ares_Module_Player_Change_Player_Side = true;
force Achilles_var_Ares_Module_Player_Create_Teleporter = true;
force Achilles_var_Ares_Module_Player_Teleport = true;
force Achilles_var_Ares_Module_Reinforcements_Create_Lz = true;
force Achilles_var_Ares_Module_Reinforcements_Create_Rp = true;
force Achilles_var_Ares_Module_Reinforcements_Spawn_Units = true;
force Achilles_var_Ares_Module_Spawn_Submarine = true;
force Achilles_var_Ares_Module_Spawn_Trawler = true;
force Achilles_var_Ares_Module_Zeus_Add_Remove_Editable_Objects = true;
force Achilles_var_Ares_Module_Zeus_Hint = true;
force Achilles_var_Ares_Module_Zeus_Switch_Side = true;
force Achilles_var_Ares_Module_Zeus_Visibility = true;
force Achilles_var_ModulePunishment_F = true;

// Achilles - Curator Vision Modes
achilles_curator_vision_blackhot = false;
achilles_curator_vision_blackhotgreencold = false;
achilles_curator_vision_blackhotredcold = false;
achilles_curator_vision_greenhotcold = false;
achilles_curator_vision_nvg = true;
achilles_curator_vision_redgreen = false;
achilles_curator_vision_redhot = false;
achilles_curator_vision_whitehot = true;
achilles_curator_vision_whitehotredcold = false;

// Achilles - Debug
Achilles_Debug_Output_Enabled = false;

// Achilles - Module Defaults
Achilles_var_setRadioFrequenciesLR_Default = "50";
Achilles_var_setRadioFrequenciesSR_Default = "150";

// Achilles - User Interface
force Achilles_var_iconSelection = "Achilles_var_iconSelection_Ares";
force Achilles_var_moduleTreeCollapse = true;
force Achilles_var_moduleTreeDLC = true;
Achilles_var_moduleTreeHelmet = false;
Achilles_var_moduleTreeSearchPatch = false;

// ADV - ACE CPR
force adv_aceCPR_addTime = 40;
force adv_aceCPR_AED_stationType = """Land_Defibrillator_F""";
force adv_aceCPR_chance_0 = 40;
force adv_aceCPR_chance_1 = 75;
force adv_aceCPR_chance_2 = 80;
force adv_aceCPR_chance_aed = 90;
force adv_aceCPR_enable = true;
force adv_aceCPR_maxTime = 3600;
force adv_aceCPR_useLocation_AED = 3;

// ADV - ACE Splint
force adv_aceSplint_enable = true;
force adv_aceSplint_patientCondition = 1;
force adv_aceSplint_reopenChance_medic = 20;
force adv_aceSplint_reopenChance_regular = 30;
force adv_aceSplint_reopenTime = 600;
force adv_aceSplint_reuseChance = 80;

// AIME Ammo Type Menu
force UPSL_aime_change_ammo_setting_ammo_class = true;
force UPSL_aime_change_ammo_setting_vehicle_ammo_class = true;

// AIME General
force UPSL_aime_setting_hide = false;

// AIME GPS and UAV Terminal
force UPSL_aime_uav_terminal_setting_gps_action = true;
force UPSL_aime_uav_terminal_setting_term_action = true;
force UPSL_aime_uav_terminal_setting_uav_action = true;

// AIME Group Management
force UPSL_aime_group_setting_drop_leader_action = true;

// AIME Inventory
force UPSL_aime_inventory_setting_assemble_action = true;
force UPSL_aime_inventory_setting_backpack_action = true;
force UPSL_aime_inventory_setting_holder_action = true;
force UPSL_aime_inventory_setting_open_action = true;

// AIME Vehicle Controls
force UPSL_aime_vehicle_controls_setting_arty_computer_action = true;
force UPSL_aime_vehicle_controls_setting_collision_action = true;
force UPSL_aime_vehicle_controls_setting_engine_action = true;
force UPSL_aime_vehicle_controls_setting_flaps_action = true;
force UPSL_aime_vehicle_controls_setting_gear_action = true;
force UPSL_aime_vehicle_controls_setting_hover_action = true;
force UPSL_aime_vehicle_controls_setting_lights_action = true;
force UPSL_aime_vehicle_controls_setting_manual_action = true;
force UPSL_aime_vehicle_controls_setting_user_actions = true;

// AIME Vehicle Seats
force UPSL_aime_vehicle_seats_setting_change_action = true;
force UPSL_aime_vehicle_seats_setting_force_eject = true;
force UPSL_aime_vehicle_seats_setting_getin_action = true;
force UPSL_aime_vehicle_seats_setting_getout_action = true;
force UPSL_aime_vehicle_seats_setting_turnout_action = true;

// ASR AI3
force asr_ai3_control_onteamswitchleader = true;
force asr_ai3_danger_AD_INSIDE = 50;
force asr_ai3_danger_AD_OUTSIDE = 250;
force asr_ai3_danger_ADVANCED_COVER = true;
force asr_ai3_danger_ATTACK_TIMER = 180;
force asr_ai3_danger_AUTO_ATTACK_WITHIN = 100;
force asr_ai3_danger_COUNTER_ATTACK = true;
force asr_ai3_danger_debug_findcover = false;
force asr_ai3_danger_debug_reveal = false;
force asr_ai3_danger_getinweapons = 0.5;
force asr_ai3_danger_MAX_DIST_TO_COVER = 300;
force asr_ai3_danger_NO_COVER_FOR_DANGER_WITHIN = 178.799;
force asr_ai3_danger_radiorange = 2000;
force asr_ai3_danger_rrdelaymin = 60;
force asr_ai3_danger_rrdelayplus = 120;
force asr_ai3_danger_seekcover = true;
force asr_ai3_danger_usebuildings = 0.8;
force asr_ai3_hitreactions_fallDown = true;
force asr_ai3_hitreactions_STAY_IN_VEH = false;
force asr_ai3_main_factionskip_str = "['LOP_AFR_Civ','LOP_CHR_Civ','LOP_TAK_Civ']";
force asr_ai3_rearming_debug_rearm = false;
force asr_ai3_rearming_rearm = 40;
force asr_ai3_rearming_rearm_fak = 1;
force asr_ai3_rearming_rearm_mags = 3;
force asr_ai3_skills_debug_setcamo = false;
force asr_ai3_skills_debug_setskill = false;
force asr_ai3_skills_packNVG = true;
force asr_ai3_skills_setskills = true;
force asr_ai3_skills_teamsuperai = true;

// Auto Weapon Lower
force FARE_ANIM_LOWER = true;
force FARE_ANIM_RAISE = true;
force FARE_ANIM_SPEEDUP = 2;
force FARE_CHECK_ADS = true;
force FARE_CHECK_LASER = true;
force FARE_CHECK_LEAN = true;
force FARE_CHECK_LIGHT = true;
force FARE_CHECK_MOVE = true;
force FARE_CHECK_RELOAD = true;
force FARE_CHECK_REST = true;
force FARE_CHECK_SHOOT = true;
force FARE_CHECK_TACTICAL = true;
force FARE_CHECK_WALK = true;
force FARE_DELAY_DAYTIME = 0.8;
force FARE_DELAY_DEFAULT = 2;
force FARE_DELAY_SHOOT = 3;
force FARE_ENABLED = true;
force FARE_STATE_DAYTIME = true;
force FARE_STATE_GEAR = true;
force FARE_STATE_MEDICAL = true;

// CBA UI
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// CBA Weapons
cba_disposable_dropUsedLauncher = 2;
force cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
cba_optics_usePipOptics = true;

// dzn Weapon Holders Carryable
force dzn_WHC_AllowedNumberOfItemsSetting = "2";
force dzn_WHC_CheckRadiusSetting = "50";
force dzn_WHC_CheckTimeoutSetting = "5";
force dzn_WHC_ClasslistSetting = "GroundWeaponHolder, WeaponHolderSimulated";
force dzn_WHC_Enabled = true;

// dzn Zeus Search Patch
dzn_ZSP_Enabled = true;
dzn_ZSP_FocusHandlerEnabled = true;
dzn_ZSP_UseExtraWideField = true;

// F/A-18
js_jc_fa18_advancedStart = false;
js_jc_fa18_canopyLoss = false;
js_jc_fa18_cursorSensitivity = 2.5;
js_jc_fa18_interactCursor = false;
js_jc_fa18_interactionRadiusMod = 1;
js_jc_fa18_showLabels = true;

// GRAD slingHelmet
force GRAD_slingHelmet_additionalList = "";
force GRAD_slingHelmet_allowAll = true;

// KAT - ACE Airway
force kat_aceAirway_checkbox_puking_sound = true;
force kat_aceAirway_deathTimer = 300;
force kat_aceAirway_enable = true;
force kat_aceAirway_probability_obstruction = 20;
force kat_aceAirway_probability_occluded = 30;
force kat_aceAirway_string_exit = "";

// KAT - ACE Breathing
force kat_aceBreathing_death_timer_enable = false;
force kat_aceBreathing_enable = true;
force kat_aceBreathing_pneumothorax = 0;
force kat_aceBreathing_spo2_big_value = 5;
force kat_aceBreathing_spo2_small_value = 2;

// KAT - ACE Circulation
kat_aceCirculation_bloodgroup = "O";
force kat_aceCirculation_enable = true;

// KAT - ACE Misc
force kat_aceMisc_enable = true;
force kat_aceMisc_limitWounds_condition = 3;
force kat_aceMisc_limitWounds_enable = true;

// KLPQ Music Radio
klpq_musicRadio_displayTilesOnLoudRadio = true;
klpq_musicRadio_enableBackpackRadioSP = false;
klpq_musicRadio_loudspeakerVolume = 2;
klpq_musicRadio_radioVolumePercent = 100;

// MRH_MilsimTools - CAS Suppport
force MRH_MilsimTools_CAS_DelayBetween = 120;
force MRH_MilsimTools_FireSupport_CAS_ConditionIsFormLeader = true;
force MRH_MilsimTools_FireSupport_CAS_CustomCondition = "(isFormationLeader MRH_player) && ([MRH_player, 'ACRE_PRC117F'] call acre_api_fnc_hasKindOfRadio)";
force MRH_MilsimTools_FireSupport_CAS_isCustomConditionSet = false;
force MRH_MilsimTools_FireSupport_CASPlanesBluFor = "B_Plane_CAS_01_dynamicLoadout_F,B_Plane_Fighter_01_F,B_UAV_02_dynamicLoadout_F,B_UAV_05_F";
force MRH_MilsimTools_FireSupport_CASPlanesInde = "I_Plane_Fighter_04_F,I_Plane_Fighter_03_dynamicLoadout_F";
force MRH_MilsimTools_FireSupport_CASPlanesOpFor = "O_Plane_Fighter_02_F,O_UAV_02_dynamicLoadout_F,O_Plane_CAS_02_dynamicLoadout_F";
force MRH_MilsimTools_FireSupport_CASShots = "5";
force MRH_MilsimTools_FireSupport_useCASSupport = false;

// MRH_MilsimTools - Core settings
force MRH_MilsimTools_AdminCasualtiesCap = 60;
force MRH_MilsimTools_AllowAdminForZeus = false;
force MRH_MilsimTools_AllowDeadReco = true;
force MRH_MilsimTools_Core_ApplyMedicPatch = true;
force MRH_MilsimTools_Delete_disconnected_body = true;
force MRH_MilsimTools_hideAceMyLoadOuts = false;
force MRH_MilsimTools_LD_loadoutDisplaySize = 100;
force MRH_MilsimTools_LD_ShowLoadOutInBrief = true;
MRH_MilsimTools_PlayIntro_ToPlayer = true;
force MRH_MilsimTools_ResetHasDiedOnRespawn = false;
force MRH_MilsimTools_Rmv_map_nolead = false;
MRH_MilsimTools_ShowAdminDeadHint = true;

// MRH_MilsimTools - Debug Mode
force MRH_MilsimTools_DebugTools_DoLogDebugMode = true;
force MRH_MilsimTools_DebugTools_isDebugMode = false;

// MRH_MilsimTools - Deployable Fobs
force MRH_MilsimTools_CAMP_composition = "MRH_SmallCamp_default";
force MRH_MilsimTools_FOB_BIG_composition = "MRH_FieldHQSmall_default";
force MRH_MilsimTools_MiscItems_CAMP_DeployTime = 10;
force MRH_MilsimTools_MiscItems_CAMP_GRepackRadius = 10;
force MRH_MilsimTools_MiscItems_CAMP_GRepackTime = 10;
force MRH_MilsimTools_MiscItems_FOB_BIGDeployTime = 20;
force MRH_MilsimTools_MiscItems_FOB_BIGRepackRadius = 50;
force MRH_MilsimTools_MiscItems_FOB_BIGRepackTime = 20;

// MRH_MilsimTools - Enhanced Map
force MRH_MilsimTools_Map_FoldUseAceLight = true;
force MRH_MilsimTools_Map_ReplaceVanillaMap = false;
force MRH_MilsimTools_Map_ZoomRatio = 0.3;

// MRH_MilsimTools - Fire Suppport
force MRH_MilsimTools_Arty_DelayBetween = 60;
force MRH_MilsimTools_FireSupport_ArtyMagazines = "32Rnd_155mm_Mo_shells,6Rnd_155mm_Mo_smoke,6Rnd_155mm_Mo_mine,2Rnd_155mm_Mo_Cluster,6Rnd_155mm_Mo_AT_mine";
force MRH_MilsimTools_FireSupport_ArtyShots = "10";
force MRH_MilsimTools_FireSupport_ConditionIsFormLeader = true;
force MRH_MilsimTools_FireSupport_CustomCondition = "(isFormationLeader MRH_player) && ([MRH_player, 'ACRE_PRC117F'] call acre_api_fnc_hasKindOfRadio)";
force MRH_MilsimTools_FireSupport_isCustomConditionSet = false;
force MRH_MilsimTools_FireSupport_useFireSupport = false;

// MRH_MilsimTools - Hacking settings
force MRH_MilsimTools_RequireHackingTool = true;

// MRH_MilsimTools - Halo Jumps
force MRH_MilsimTools_HaloGear_AADDefaultOpeningAltitude = "900";
force MRH_MilsimTools_HaloGear_allowManualAADSetting = true;
force MRH_MilsimTools_HaloGear_disableMaskHud = false;
force MRH_MilsimTools_HaloGear_disableMaskHudAltimeter = false;
force MRH_MilsimTools_HaloGear_disableMaskHudCompass = false;
force MRH_MilsimTools_HaloGear_disableMaskHudVelocity = false;
force MRH_MilsimTools_HaloGear_haloMaskBreakingChancePerc = 10;
force MRH_MilsimTools_HaloGear_hypoxiaAltitude = 4600;
force MRH_MilsimTools_HaloGear_hypoxiaUse = true;
force MRH_MilsimTools_HaloGear_missionConfiguredProtectiveGear = "RHS_jetpilot_usaf,rhs_zsh7a,rhs_zsh7a_alt,RHS_TU95MS_vvs_old";
force MRH_MilsimTools_HaloGear_playRebreatherSounds = true;

// MRH_MilsimTools - Heli Taxi
force MRH_MilsimTools_BluForCivs = "C_Heli_Light_01_civil_F";
force MRH_MilsimTools_BluForHelis = "B_Heli_Transport_01_F,B_Heli_Light_01_F,B_Heli_Transport_03_unarmed_F,B_T_VTOL_01_infantry_F";
force MRH_MilsimTools_BluForInde = "I_Heli_Transport_02_F,I_Heli_light_03_unarmed_F";
force MRH_MilsimTools_BluForOpFor = "O_Heli_Transport_04_bench_F,O_Heli_Light_02_unarmed_F,O_Heli_Transport_04_covered_F";
force MRH_MilsimTools_Heli_ConditionIsFormLeader = true;
force MRH_MilsimTools_Heli_CustomCondition = "(isFormationLeader MRH_player) && ([MRH_player, 'ACRE_PRC117F'] call acre_api_fnc_hasKindOfRadio)";
force MRH_MilsimTools_Heli_isCustomConditionSet = false;
force MRH_MilsimTools_Heli_NumberSimulTPerSide = "3";
force MRH_MilsimTools_Heli_UseHeliTaxiInMission = false;

// MRH_MilsimTools - Insertion Handler
force MRH_MilsimTools_InsertionHandler_allowHALO = true;
force MRH_MilsimTools_InsertionHandler_allowStatic = true;
force MRH_MilsimTools_InsertionHandler_allowSub = true;
force MRH_MilsimTools_InsertionHandler_SupplyPlanesBluFor = "B_T_VTOL_01_infantry_F,B_Heli_Transport_03_unarmed_F,B_Heli_Transport_01_F";
force MRH_MilsimTools_InsertionHandler_SupplyPlanesInde = "I_Heli_Transport_02_F,I_Heli_light_03_unarmed_F,I_C_Plane_Civil_01_F";
force MRH_MilsimTools_InsertionHandler_SupplyPlanesOpFor = "O_T_VTOL_02_infantry_dynamicLoadout_F,O_Heli_Light_02_unarmed_F,O_Heli_Transport_04_box_F";

// MRH_MilsimTools - JIP settings
force MRH_MilsimTools_Jip_Menu_sideOnly = true;
force MRH_MilsimTools_Jip_MenuAllow = true;
force MRH_MilsimTools_Jip_MenuIncludeAI = true;

// MRH_MilsimTools - MEDEVAC
force MRH_MilsimTools_MEDEVAC_BluFor = "B_Heli_Light_01_F,B_Heli_Transport_01_F,B_Heli_Transport_03_unarmed_F";
force MRH_MilsimTools_MEDEVAC_Civs = "C_IDAP_Heli_Transport_02_F,C_Heli_Light_01_civil_F";
force MRH_MilsimTools_MEDEVAC_ConditionIsFormLeader = true;
force MRH_MilsimTools_MEDEVAC_CustomCondition = "(isFormationLeader MRH_player) && ([MRH_player, 'ACRE_PRC117F'] call acre_api_fnc_hasKindOfRadio)";
force MRH_MilsimTools_MEDEVAC_DelayBetween = 120;
force MRH_MilsimTools_MEDEVAC_ESCORT_BluFor = "B_Heli_Attack_01_dynamicLoadout_F";
force MRH_MilsimTools_MEDEVAC_ESCORT_BluForCivs = "C_Heli_Light_01_civil_F";
force MRH_MilsimTools_MEDEVAC_ESCORT_Inde = "I_Heli_light_03_dynamicLoadout_F";
force MRH_MilsimTools_MEDEVAC_ESCORT_OpFor = "O_Heli_Attack_02_dynamicLoadout_F";
force MRH_MilsimTools_MEDEVAC_Inde = "I_Heli_light_03_unarmed_F,I_Heli_Transport_02_F";
force MRH_MilsimTools_MEDEVAC_isCustomConditionSet = false;
force MRH_MilsimTools_MEDEVAC_MEDICCLASS_BluFor = "B_medic_F";
force MRH_MilsimTools_MEDEVAC_MEDICCLASS_Civs = "C_Man_Paramedic_01_F";
force MRH_MilsimTools_MEDEVAC_MEDICCLASS_Inde = "I_medic_F";
force MRH_MilsimTools_MEDEVAC_MEDICCLASS_ItemsCall = "ACE_HandFlare_Red,SmokeShellOrange";
force MRH_MilsimTools_MEDEVAC_MEDICCLASS_OpFor = "O_medic_F";
force MRH_MilsimTools_MEDEVAC_OpFor = "O_Heli_Light_02_dynamicLoadout_F,O_Heli_Transport_04_medevac_F";
force MRH_MilsimTools_MEDEVAC_timeToReachLZ = 600;
force MRH_MilsimTools_MEDEVAC_UseMedEVACInMission = false;

// MRH_MilsimTools - Medical Tent
force MRH_MilsimTools_MiscItems_FieldMedicalTentDeployTime = 20;
force MRH_MilsimTools_MiscItems_FieldMedicalTentRepackTime = 10;

// MRH_MilsimTools - Roster settings
force MRH_MilsimTools_Roster_AllowAllSides = false;
force MRH_MilsimTools_Roster_ColorBF = [0.259,0.525,0.957,1];
force MRH_MilsimTools_Roster_ColorCiv = [0.686,0.141,0.588,1];
force MRH_MilsimTools_Roster_ColorInd = [0.212,0.847,0.286,1];
force MRH_MilsimTools_Roster_ColorOp = [0.929,0.063,0.063,1];
force MRH_MilsimTools_Roster_ColorUnknown = [0.929,0.82,0.118,1];
force MRH_MilsimTools_Roster_ShowAIgroups = false;
force MRH_MilsimTools_Roster_ShowAiWithinGrps = true;

// MRH_MilsimTools - Supply drops
force MRH_MilsimTools_FireSupport_NumberOfSuppliesDrops = "3";
force MRH_MilsimTools_FireSupport_Supplies_ConditionIsFormLeader = true;
force MRH_MilsimTools_FireSupport_Supplies_CustomCondition = "(isFormationLeader MRH_player) && ([MRH_player, 'ACRE_PRC117F'] call acre_api_fnc_hasKindOfRadio)";
force MRH_MilsimTools_FireSupport_Supplies_DistanceForAircraft = "2000";
force MRH_MilsimTools_FireSupport_Supplies_isCustomConditionSet = false;
force MRH_MilsimTools_FireSupport_Supplies_ListOfAvailableSupplies = "B_LSV_01_unarmed_F,ACE_Box_82mm_Mo_Combo,ACE_medicalSupplyCrate_advanced,ACE_Box_Misc,B_supplyCrate_F";
force MRH_MilsimTools_FireSupport_Supplies_useSupplyDrops = false;
force MRH_MilsimTools_FireSupport_SupplyPlanesBluFor = "B_T_VTOL_01_vehicle_F,B_Heli_Transport_03_unarmed_F,B_Heli_Transport_01_F";
force MRH_MilsimTools_FireSupport_SupplyPlanesInde = "I_Heli_Transport_02_F,I_Heli_light_03_unarmed_F,I_C_Plane_Civil_01_F";
force MRH_MilsimTools_FireSupport_SupplyPlanesOpFor = "O_T_VTOL_02_vehicle_dynamicLoadout_F,O_Heli_Light_02_unarmed_F,O_Heli_Transport_04_box_F";
force MRH_MilsimTools_SupplyDrop_DelayBetween = 600;

// MRH_MilsimTools - Tablet settings
force MRH_MilsimTools_AllowMapTablet = true;
force MRH_MilsimTools_ST_BriefingRemote_overlay = "\MRHMilsimTools\Paa\cnn_news_ca.paa";
force MRH_MilsimTools_ST_BriefingRemote_videosToPlay = "\a3\missions_f_bootcamp\video\vr_generictransition_1.ogv,\a3\missions_f_epa\video\a_in_intro.ogv";

// MRH_MilsimTools - Zeus
MRH_MilsimTools_Zeus_hideWaterMark = false;
MRH_MilsimTools_Zeus_waterMark = "UseVanilla";
MRH_MilsimTools_Zeus_waterMarkCustomImg = "\MRHMilsimTools\Paa\zeusicon.paa";

// MRHSatellite Options
force MRH_SAT_allowFullscreen = true;
force MRH_SAT_allowLasering = true;
force MRH_SAT_allowTargetDetection = true;
force MRH_SAT_allowTargetTracking = true;
force MRH_SAT_MaxSatAltitude = 600;

// MRHSpawner Allowed DLCs
force apalon team = true;
force ej_Uh60 = true;
force Enoch = true;
force Expansion = true;
force Heli = true;
force Jets = true;
force Kart = true;
force LOP_LeightsOPFOR = true;
force ORANGE = true;
force RHS_AFRF = true;
force RHS_GREF = true;
force RHS_SAF = true;
force RHS_USAF = true;
force Tank = true;
force u100 = true;
force Vanilla = true;

// MRHSpawner Allowed Factions
force B_UnitedStatesUSNAVY_Clandestine = true;
force BLU_CTRG_F = true;
force blu_F = true;
force BLU_G_F = true;
force BLU_GEN_F = true;
force BLU_T_F = true;
force BRPMC = true;
force CIV_F = true;
force CIV_IDAP_F = true;
force DSF_ABUS = true;
force DSF_AFMIL = true;
force DSF_BOKO = true;
force DSF_CART = true;
force DSF_IDF = true;
force DSF_ISIS = true;
force DSF_TAL = true;
force FIR_AWS_ENEMY_F = true;
force IND_C_F = true;
force IND_E_F = true;
force IND_F = true;
force IND_G_F = true;
force LOP_AA = true;
force LOP_AFR = true;
force LOP_AFR_Civ = true;
force LOP_AFR_OPF = true;
force LOP_AM = true;
force LOP_AM_OPF = true;
force LOP_BH = true;
force LOP_CDF = true;
force LOP_ChDKZ = true;
force LOP_CHR_Civ = true;
force LOP_GRE = true;
force LOP_IA = true;
force LOP_IRA = true;
force LOP_IRAN = true;
force LOP_ISTS = true;
force LOP_ISTS_OPF = true;
force LOP_NAPA = true;
force LOP_NK = true;
force LOP_PESH = true;
force LOP_PESH_IND = true;
force LOP_PMC = true;
force LOP_RACS = true;
force LOP_SLA = true;
force LOP_SYR = true;
force LOP_TAK_Civ = true;
force LOP_TKA = true;
force LOP_TRK = true;
force LOP_UA = true;
force LOP_UKR = true;
force LOP_UN = true;
force LOP_US = true;
force LOP_UVF = true;
force opf_F = true;
force OPF_G_F = true;
force OPF_T_F = true;
force rhs_faction_msv = true;
force rhs_faction_rva = true;
force rhs_faction_socom = true;
force rhs_faction_tv = true;
force rhs_faction_usaf = true;
force rhs_faction_usarmy_d = true;
force rhs_faction_usarmy_wd = true;
force rhs_faction_usmc_d = true;
force rhs_faction_usmc_wd = true;
force rhs_faction_vdv = true;
force rhs_faction_vmf = true;
force rhs_faction_vpvo = true;
force rhs_faction_vv = true;
force rhs_faction_vvs = true;
force rhs_faction_vvs_c = true;
force rhsgref_faction_cdf_air = true;
force rhsgref_faction_cdf_air_b = true;
force rhsgref_faction_cdf_ground = true;
force rhsgref_faction_cdf_ground_b = true;
force rhsgref_faction_chdkz = true;
force rhsgref_faction_chdkz_g = true;
force rhsgref_faction_hidf = true;
force rhsgref_faction_nationalist = true;
force rhsgref_faction_tla = true;
force rhsgref_faction_un = true;
force rhssaf_faction_airforce = true;
force rhssaf_faction_airforce_opfor = true;
force rhssaf_faction_army = true;
force rhssaf_faction_army_opfor = true;
force rhssaf_faction_un = true;
force RWG = true;
force TF160 = true;
force USA = true;
force USAF = true;
force USMC = true;
force USN = true;

// MrSanchez' Headlamp
SAN_Headlamp_AI_UpdateRate = 30;
force SAN_Headlamp_Arcade = true;
SAN_Headlamp_Multiplayer_UpdateRate = 30;
force SAN_Headlamp_RenderDistance = 1500;
SAN_Headlamp_SoundClickEnabled = true;

// NIArms
force niarms_accswitch = true;
force niarms_magSwitch = true;

// NMAB Settings
NMAB_setting_particlesEnabled = true;

// STUI Settings
force STGI_Settings_Enabled = true;
force STGI_Settings_UnconsciousFadeEnabled = true;
force STHud_Settings_ColourBlindMode = "Normal";
force STHud_Settings_Font = "PuristaSemibold";
force STHud_Settings_HUDMode = 1;
force STHud_Settings_Occlusion = true;
force STHud_Settings_RemoveDeadViaProximity = false;
force STHud_Settings_SquadBar = true;
force STHud_Settings_TextShadow = 1;
force STHud_Settings_UnconsciousFadeEnabled = true;

// TacSalmon Buttstroke
Salmon_bs_ff = true;
Salmon_bs_rd = true;

// Tactical Position Ready Options
Animation for automatic wall avoidance = 0;
Change the position recovery from left click to right click. = false;
Enable automatic wall avoidance. = true;
Lookahead for corner detection (in m) = "0.2";

// Tactical Weapon Swap Option
Swap the gun quickly without an animation process. = true;
You can wear another weapon in the state of a pistol. = false;

// Task Force Arrowhead Radio
force TF_default_radioVolume = 9;
force TF_give_microdagr_to_soldier = true;
force TF_give_personal_radio_to_regular_soldier = true;
force TF_no_auto_long_range_radio = true;
force TF_same_dd_frequencies_for_side = true;
force TF_same_lr_frequencies_for_side = true;
force TF_same_sw_frequencies_for_side = true;

// VCOM SETTINGS
force VCM_ActivateAI = true;
force VCM_ADVANCEDMOVEMENT = true;
force VCM_AIDISTANCEVEHPATH = 100;
force VCM_AIMagLimit = 2;
force VCM_AISNIPERS = true;
force VCM_AISUPPRESS = true;
force VCM_ARTYDELAY = 30;
force VCM_ARTYENABLE = false;
force VCM_ARTYSIDES = [WEST,EAST,GUER];
force VCM_CARGOCHNG = true;
force VCM_ClassSteal = true;
force VCM_Debug = false;
force VCM_DISEMBARKRANGE = 800;
force Vcm_DrivingActivated = true;
force VCM_ForceSpeed = true;
force VCM_FRMCHANGE = true;
force VCM_HEARINGDISTANCE = 5000;
force VCM_MINECHANCE = 1.20901;
force Vcm_PlayerAISkills = true;
force VCM_RAGDOLL = false;
force VCM_RAGDOLLCHC = 0;
force VCM_SIDEENABLED = [WEST,EAST,GUER];
force VCM_SKILLCHANGE = true;
force VCM_STATICARMT = 300;
force VCM_StealVeh = true;
force VCM_TURRETUNLOAD = true;
force VCM_USECBASETTINGS = true;
force VCM_WARNDELAY = 600;
force VCM_WARNDIST = 8000;

// VET_Unflipping
force vet_unflipping_require_serviceVehicle = false;
force vet_unflipping_require_toolkit = false;
force vet_unflipping_time = 5;
force vet_unflipping_unit_man_limit = 7;
force vet_unflipping_unit_mass_limit = 3000;
force vet_unflipping_vehicle_mass_limit = 100000;

// White Phosphor
force Fat_Lurch_UseWP = true;
